Shader "Added/ColoredAlpha" {
    Properties {
        _Color ("Color(RGB)", Color) = (1,1,1)
        _MainTex ("Main Tex", 2D) = "white" {}
    }
	Category {
		Tags {Queue=Transparent}
		Lighting Off
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		
		SubShader {	
			Pass {
			   SetTexture [_MainTex] {
			   		ConstantColor [_Color]
					Combine constant, constant * texture
			    }
			}
        } 
    }
}