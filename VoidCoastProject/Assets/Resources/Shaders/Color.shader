Shader "Added/Colored" {
Properties {
	_Color ("Color", Color) = (1,1,1,1)
}

Category {
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	Blend SrcAlpha OneMinusSrcAlpha
	Lighting Off Cull Back ZTest Always ZWrite Off 
	
	SubShader {
		Pass {
			Color [_Color]
		}
	}
}
}