using UnityEngine;
using System.Collections;

public class TextEffect : MonoBehaviour
{
	TextMeshLabel label;
	string text;
	const float maxLifeTime = 1.8f;
	float lifeTimer = maxLifeTime;
	Vector3 velocity;
	Color color = Color.white;
	public static Camera faceCamera;

	public static void Show (Vector3 position, string text, Color color, Vector3 moveUpVelocity)
	{
		TextEffect effect = Utils.Create<TextEffect> (null, position);
		effect.text = text;
		effect.color = color;
		effect.velocity = moveUpVelocity;
	}

	void Start ()
	{
		faceCamera = Camera.main;
		label = TextMeshLabel.Create (transform, Vector3.zero, text, 0.2f);
		color.a = 0;
		label.SetColor (color);
		label.gameObject.AddComponent<SetRenderQueue> ();
	}
	
	void Update ()
	{
		if (lifeTimer > 0) {
			lifeTimer -= Time.deltaTime;
			if (lifeTimer < 0) {
				Destroy (gameObject);
			}
			if (lifeTimer < maxLifeTime / 5f) {
				color.a = lifeTimer / (maxLifeTime / 5f);
			} else {
				if (lifeTimer > maxLifeTime / 5f * 4) {
					color.a = (maxLifeTime - lifeTimer) / (maxLifeTime / 5f);
				} else {
					color.a = 1;
				}
			}
			label.SetColor (color);
		}
		transform.localPosition += velocity * Time.deltaTime;
		transform.localRotation = faceCamera.transform.rotation;
	}
}
