using UnityEngine;
using System.Collections;

public class FPS : MonoBehaviour {
	static FPS instance;
    float timeA; 
    int fps;
    int lastFPS;
    public GUIStyle textStyle;
	
	void Awake () {
		if (instance != null) {
			Destroy(this);
		} else {
			instance = this;
		}
	}
	
	void Start () {
        timeA = Time.timeSinceLevelLoad;
		textStyle.fontSize = 40;
		DontDestroyOnLoad (this);
    }

	void Update () {
	    if(Time.timeSinceLevelLoad  - timeA <= 1){
            fps++;
        } else{
            lastFPS = fps + 1;
			timeA = Time.timeSinceLevelLoad;
	        fps = 0;
        }
    }

    void OnGUI(){
        GUI.Label(new Rect( 10, 10, 50, 50), lastFPS.ToString(), textStyle);
    }
}