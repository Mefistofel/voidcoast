﻿using UnityEngine;
using System.Collections;

// for demos
public class Rotator : MonoBehaviour {

	[SerializeField] public Vector3 axe = Vector3.up;
	[SerializeField] public float speed = 1f; // angles per second

	Transform thisTransform;

	void Start () {
		thisTransform = transform;
	}

	void Update () {
		thisTransform.Rotate (axe, speed * Time.deltaTime);
	}
}
