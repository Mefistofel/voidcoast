﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MissionController : MonoBehaviour
{
	public string descriptionText = "";
	public bool isStarted = false;

	public List<Condition> missionStartConditions = new List<Condition>();
	public List<Condition> missionSuccessConditions = new List<Condition>();
	public List<ConsumptionResource> missionBounty = new List<ConsumptionResource>();

	void Start()
	{
		Stock.Subscribe (OnSomeResourceChangedDelegate);
	}

	void OnSomeResourceChangedDelegate()
	{
		if (!isStarted)
		{
			bool conditionsToStart = true;
			foreach(var condition in missionStartConditions)
			{
				if (!condition.Check())
				{
					conditionsToStart = false;
				}
			}
			if (conditionsToStart)
			{
				// start mission
			}
		}
	}
}
