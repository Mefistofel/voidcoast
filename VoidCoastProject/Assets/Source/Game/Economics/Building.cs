﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public enum BuildingType{
	DummyBuilding = 0,
	CommandCenter = 1,
	WaterExtractor = 2,
	OreMine = 3,
	Habitat = 4,
	HydroponicFarm = 5,
	StarPort = 6,
	ChemicalPlant = 7,
	SolarPlant = 8,
	Factory = 9
}

/// <summary>
/// Controller for building.
/// </summary>
public class Building : MonoBehaviour
{
	public string name = ""; // set for prefab from editor
	public int needTechLevel = 0;
	public BuildingType type = BuildingType.DummyBuilding;
	public List<ConsumptionResource> cost = new List<ConsumptionResource>() { new ConsumptionResource("money", 100)};

	public bool active = false;

	Vector3 upVector = Vector3.zero;

	public static Building CreateTemplate(BuildingType type)
	{
		GameObject prefabObject = (GameObject)Instantiate (Resources.Load ("Prefabs/" + type.ToString()));
		Building build = prefabObject.GetComponent<Building> ();
		if (build != null)
		{
			build.type = type;
		}
		return build;
	}

	public void Build(Vector3 up)
	{
		upVector = up;
		// pay for building
		foreach (var resourse in cost)
		{
			Stock.resources[resourse.name] -= resourse.value;
		}
		// activate building
		if (!active)
		{
			active = true;
			foreach(Consumption consume in transform.GetComponents<Consumption>())
			{
				consume.SubscribeSlotResourcesConsume();
			}
		}
	}

	public void ShowTextEffect(string text, int resCount)
	{
		Vector3 start = transform.position + upVector * (0.4f);
		Vector3 up = upVector;
		Timer.Add (0.05f + 0.35f * (float)resCount,
		           () => {
			TextEffect.Show (
				start,
				text,
				Color.white,
				up);
		});
	}

	void Start ()
	{
		Stock.resources [type.ToString ()] += 1;
	}
	
	void OnDestroy ()
	{
		Stock.resources [type.ToString ()] -= 1;
		if (active)
		{
			active = false;
			foreach(Consumption consume in transform.GetComponents<Consumption>())
			{
				consume.UnsubscribeSlotResourcesConsume();
			}
		}
	}

	void Update ()
	{
	
	}
}
