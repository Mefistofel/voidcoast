﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Resource stock. Main module with resources list 
/// </summary>
public class Stock
{
	public static Stock resources = new Stock ();
	public Dictionary<string, int> resourceList = new Dictionary<string, int> ();

	public delegate void OnSomeResourcesCountChange();
	public delegate void OnResourceCountChange(int newCount);
	Dictionary<string, OnResourceCountChange> subscriptions = new Dictionary<string, OnResourceCountChange>();
	OnSomeResourcesCountChange allSubscription = () => {};

	public int this [string resourceName] {
		get {
			if (resourceList.ContainsKey (resourceName)) {
				return resourceList [resourceName];
			}
			return 0;
		}
		set {
			if (!resourceList.ContainsKey (resourceName)) {
				resourceList.Add (resourceName, value);
				OnResourceCountChanging(resourceName, value);
			} else {
				if (resourceList[resourceName] != value)
				{
					resourceList[resourceName] = value;
					OnResourceCountChanging(resourceName, value);
				}
			}
		}
	}
	
	void OnResourceCountChanging(string resourceName, int newValue)
	{
		GameUI.OnStateChanged();
		if (subscriptions.ContainsKey (resourceName))
		{
			if (subscriptions[resourceName] != null)
			{
				subscriptions[resourceName](newValue);
			}
			else
			{
				subscriptions.Remove(resourceName);
			}
		}
		allSubscription ();
	}

	public static void Subscribe(string resourceName, OnResourceCountChange onChangeDelegate)
	{
		if (resources != null && onChangeDelegate != null)
		{
			if (!resources.subscriptions.ContainsKey (resourceName))
			{
				resources.subscriptions.Add(resourceName, onChangeDelegate);
			}
			else
			{
				resources.subscriptions[resourceName] += onChangeDelegate;
			}
			onChangeDelegate(resources[resourceName]);
		}
	}
	public static void Subscribe(OnSomeResourcesCountChange onChangeDelegate)
	{
		if (resources != null && onChangeDelegate != null) {
			resources.allSubscription += onChangeDelegate;
		}
	}
}
