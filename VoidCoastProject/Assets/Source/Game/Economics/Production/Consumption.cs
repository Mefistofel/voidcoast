﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ConsumptionResource // consume or produse slot resource
{
	public string name;
	public int value = 1;
	
	public ConsumptionResource() {}

	public ConsumptionResource(string newName, int newValue)
	{
		name = newName;
		value = newValue;
	}
}
/// <summary>
/// Consumption - create or consume slot resources.
/// </summary>
public class Consumption : MonoBehaviour
{
	Building controlledBuilding = null;
	#if UNITY_EDITOR
	public string productionName = "Consumption name"; // description name for editor
	#endif
	// consume or produse slot resources always - energy for example
	public List<ConsumptionResource> slotResourcesConsume = new List<ConsumptionResource>();

	void Awake ()
	{
		controlledBuilding = GetComponent<Building> ();
		if (controlledBuilding == null)
		{
			controlledBuilding = transform.parent.gameObject.GetComponent<Building>();
		}
	}

	public void SubscribeSlotResourcesConsume()
	{
		if (slotResourcesConsume != null)
		{
			int resCount = 0;
			foreach(var consumeResource in slotResourcesConsume)
			{
				Stock.resources[consumeResource.name] += consumeResource.value;
				if (controlledBuilding != null)
				{
					controlledBuilding.ShowTextEffect(consumeResource.name + " " + consumeResource.value.ToString(), resCount);
				}
				resCount++;
			}
		}
	}
	
	public void UnsubscribeSlotResourcesConsume()
	{
		if (slotResourcesConsume != null)
		{
			int resCount = 0;
			foreach(var consumeResource in slotResourcesConsume)
			{
				Stock.resources[consumeResource.name] -= consumeResource.value;
				if (canShowEffect)
				{
					if (controlledBuilding != null)
					{
						controlledBuilding.ShowTextEffect(consumeResource.name + " " + (-consumeResource.value).ToString(), resCount);
					}
				}
				resCount++;
			}
		}
	}

	static bool canShowEffect = true;
	void OnApplicationQuit ()
	{
		canShowEffect = false;
	}
}
