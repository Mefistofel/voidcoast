﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ProductionResource // produce #value of "name" resource in production time
{
	public string name;
	public string value = "1"; // value can be int constant or another resource name
	public int Value
	{
		get
		{
			int result = 0;
			if (!int.TryParse(value, out result))
			{
				return Stock.resources[value];
			}
			return result;
		}
	}
}
/// <summary>
/// Produse usual resources if condition checked
/// </summary>
public class Production : MonoBehaviour
{
#if UNITY_EDITOR
	public string productionName = "production name"; // description name for editor
#endif

	public float productionTime = 1f; // full time of production circle 
	public List<ProductionResource> consumptionResources = new List<ProductionResource>(); // change on start of production circle
	public List<ProductionResource> productionResources = new List<ProductionResource>(); // change on end of production circle
	// if conditions check is true - then production make operations form production list
	public List<Condition> workConditions = new List<Condition>();
	
	const float defaultDowntime = 0.5f; // time to wait if condition is invalid
	float timer = 0;
	bool productionCircleStarted = false;

	Building controlledBuilding = null;

	void Start ()
	{
		timer = Random.Range (0, 0.5f); // for random start time
		controlledBuilding = GetComponent<Building> ();
		if (controlledBuilding == null)
		{
			controlledBuilding = transform.parent.gameObject.GetComponent<Building>();
		}
	}

	void OnDestroy ()
	{
	}

	void Update ()
	{
		if (controlledBuilding == null || !controlledBuilding.active)
		{
			return;
		}
		if (productionTime <= 0)
		{
			return;
		}
		timer -= Time.deltaTime * Game.gameSpeed;
		if (timer < 0)
		{
			if (productionCircleStarted)
			{
				EndProductionCircle();
			}
			if (CheckConditions())
			{
				StartProductionCircle();
			}
			else
			{
				timer = defaultDowntime;
			}
		}
	}

	public bool CheckConditions()
	{
		foreach(var condition in workConditions)
		{
			if (!condition.Check())
			{
				// TODO here must message about conditions fail
				return false;
			}
		}
		return true;
	}
	
	public void StartProductionCircle()
	{
		productionCircleStarted = true;
		foreach(var consumeResource in consumptionResources)
		{
			Stock.resources[consumeResource.name] += consumeResource.Value;
		}
		timer += productionTime; 
	}
	
	public void EndProductionCircle()
	{
		productionCircleStarted = false;
		int resCount = 0;
		foreach(var incomeResource in productionResources)
		{
			Stock.resources[incomeResource.name] += incomeResource.Value;
			if (controlledBuilding != null)
			{
				controlledBuilding.ShowTextEffect( incomeResource.name + " " + (incomeResource.Value > 0?"+":"") + incomeResource.Value.ToString(), resCount);
			}
			resCount++;
		}
	}

	static bool canShowEffect = true;
	void OnApplicationQuit ()
	{
		canShowEffect = false;
	}
}
