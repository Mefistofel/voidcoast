﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum ConditionLogic
{
	Equal,
	NonEqual,
	Less,
	EqualOrLess,
	Greater,
	EqualOrGreater,
}

[System.Serializable]
public class Condition
{
	public string resourceName;
	public ConditionLogic logic = ConditionLogic.Equal;
	public string secondArgumentName;

	public Condition(string resource, ConditionLogic conditionLogic, int newConstant = 0)
	{
		resourceName = resource;
		logic = conditionLogic;
		secondArgumentName = newConstant.ToString();
	}

	public Condition(string resource, ConditionLogic conditionLogic, string secondArgument)
	{
		resourceName = resource;
		logic = conditionLogic;
		secondArgumentName = secondArgument;
	}

	public bool Check()
	{
		// if second argument can be parsed as int - its numeric, else its resource name
		int secondArgumentValue;
		if (!int.TryParse (secondArgumentName, out secondArgumentValue))
		{
			secondArgumentValue = Stock.resources[secondArgumentName];
		}
		switch (logic)
		{
		default:
		case ConditionLogic.Equal:
			return Stock.resources[resourceName] == secondArgumentValue;
		case ConditionLogic.NonEqual:
			return Stock.resources[resourceName] != secondArgumentValue;
		case ConditionLogic.Less:
			return Stock.resources[resourceName] < secondArgumentValue;
		case ConditionLogic.EqualOrLess:
			return Stock.resources[resourceName] <= secondArgumentValue;
		case ConditionLogic.Greater:
			return Stock.resources[resourceName] > secondArgumentValue;
		case ConditionLogic.EqualOrGreater:
			return Stock.resources[resourceName] >= secondArgumentValue;
		}
	}
}
