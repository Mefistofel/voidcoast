﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour
{
	static Game instance = null;
	public static float gameSpeed = 1f;

	void Awake ()
	{
		instance = this;
		Stock.resources ["command_module"] = 1;
		Stock.resources ["money"] = 5000;
		Stock.resources ["water"] = 20;
		Stock.resources ["materials"] = 15;
	}
	
	// Update is called once per frame
	void Update ()
	{
	}
}