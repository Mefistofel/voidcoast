﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
	[SerializeField]Camera controlledCamera = null;// set from editor

	[SerializeField]float xFactor = 1000f;
	[SerializeField]float yFactor = -1000f;
	[SerializeField]float maxDistance = 50f;
	[SerializeField]float minDistance = 11f;
	[SerializeField]float changeViewDistance = 15f;
	[SerializeField]float wheelScaleFactor = 8f;

	float currentCameraDistance = -3f;
	[SerializeField]float needCameraDistance = -18f;
	Quaternion needCameraRotation = Quaternion.identity;

	Vector2 lastMousePosition = Vector2.zero;
	Transform selfTransform = null;

	void Start ()
	{
		selfTransform = transform;
		if (controlledCamera != null)
		{
			currentCameraDistance = controlledCamera.transform.localPosition.z;
			needCameraRotation = transform.localRotation;
		}
	}

	void Update ()
	{
		// rotate
		if (Input.GetMouseButtonDown (1)) {
			lastMousePosition = Input.mousePosition;
		}
		if (Input.GetMouseButton (1)) {
			needCameraRotation *= Quaternion.Euler (
				(Input.mousePosition.y - lastMousePosition.y) / Screen.height * yFactor,
				(Input.mousePosition.x - lastMousePosition.x) / Screen.width * xFactor,
				0);
			lastMousePosition = Input.mousePosition;
		}
		const float keyboardMoveSpeed = 80f;
		const float keyboardRotationSpeed = 120f;
		if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.UpArrow))
		{
			needCameraRotation *= Quaternion.Euler (Time.deltaTime * keyboardMoveSpeed, 0, 0);
		}
		if (Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.DownArrow))
		{
			needCameraRotation *= Quaternion.Euler (-Time.deltaTime * keyboardMoveSpeed, 0, 0);
		}
		if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow))
		{
			needCameraRotation *= Quaternion.Euler (0, Time.deltaTime * keyboardMoveSpeed, 0);
		}
		if (Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow))
		{
			needCameraRotation *= Quaternion.Euler (0, -Time.deltaTime * keyboardMoveSpeed, 0);
		}
		if (Input.GetKey (KeyCode.Q))
		{
			needCameraRotation *= Quaternion.Euler (0, 0, Time.deltaTime * keyboardRotationSpeed);
		}
		if (Input.GetKey (KeyCode.E))
		{
			needCameraRotation *= Quaternion.Euler (0, 0, -Time.deltaTime * keyboardRotationSpeed);
		}
		selfTransform.localRotation = Quaternion.Lerp (selfTransform.localRotation, needCameraRotation, 0.1f);
		ScaleCamera (Input.GetAxis ("Mouse ScrollWheel"));
		currentCameraDistance = currentCameraDistance * 0.9f + needCameraDistance * 0.1f;
		controlledCamera.transform.localPosition = new Vector3 (0, 0, currentCameraDistance);
		float changeCameraView = 0;
		if (currentCameraDistance > -changeViewDistance)
		{
			changeCameraView = ((currentCameraDistance + minDistance) - (-changeViewDistance + minDistance)) / (minDistance - changeViewDistance);
			changeCameraView = changeCameraView * 50f;
		}
		controlledCamera.transform.localEulerAngles = new Vector3 (changeCameraView, 0, 0);
	}

	void ScaleCamera(float wheelDelta) {
		needCameraDistance += wheelDelta * wheelScaleFactor;
		if (needCameraDistance > -minDistance)
		{
			needCameraDistance = -minDistance;
		}
		if (needCameraDistance < -maxDistance)
		{
			needCameraDistance = -maxDistance;
		}
	}
}
