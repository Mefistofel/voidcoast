﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AsteroidMesh : MonoBehaviour
{
	[SerializeField] float size = 50f;
	[SerializeField] private Material material;

	protected MeshFilter meshFilter;
	protected MeshRenderer meshRenderer;
	protected MeshCollider meshCollider;

	public Vector3[] navMeshPoints = null;
	public Vector3[] navMeshNormals = null;
	public int[][] navMeshLinks = null;
	public Vector3[] navMeshCollider = null;

	#if UNITY_EDITOR
	void OnDrawGizmos ()
	{
		if (navMeshPoints != null) 
		{
			for(int i = 0; i < navMeshPoints.Length; i++)
			{
				Gizmos.DrawCube(navMeshPoints[i], Vector3.one * 0.02f);
//				foreach(var neighbor in navMeshLinks[i])
//				{
//					Gizmos.DrawLine(navMeshPoints[i], navMeshPoints[neighbor]);
//				}
				Gizmos.DrawLine(navMeshPoints[i], navMeshPoints[i] + navMeshNormals[i] * 0.1f);
			}
		}
		/// DrawUV
		for(int i =0; i < UVTriangles.Length / 3; i++)
		{
			for( int point = 0; point < 3; point ++)
			{
				Gizmos.DrawLine(baseUV[UVTriangles[i * 3 + point]], baseUV[UVTriangles[i * 3 + (point + 1) % 3]]);
			}
		}
		Gizmos.DrawLine (new Vector2(2.5f, 5f), new Vector2(0, 5f));
		Gizmos.DrawLine (new Vector2(2.5f, 5f), new Vector2(2.5f, 0));
	}
	#endif

	void Awake ()
	{
		meshRenderer = Utils.GetOrCreateComponent<MeshRenderer> (gameObject);
		meshFilter = Utils.GetOrCreateComponent<MeshFilter> (gameObject);
		meshCollider = Utils.GetOrCreateComponent<MeshCollider> (gameObject);
	}

	void Start ()
	{
		if (material != null) {
			meshRenderer.material = material;
		}
		meshFilter.mesh = CreateMesh ();
		
		List<Vector3> navs = new List<Vector3>();
		List<Vector3> navNormals = new List<Vector3>();
		List<List<int>> neighbors = new List<List<int>>();
		for (int i = 0; i < triangles.Length / 3; i++)
		{
			subdivideNavPoints(
				baseVertices[triangles[i * 3 + 0]].normalized,
				baseVertices[triangles[i * 3 + 1]].normalized,
				baseVertices[triangles[i * 3 + 2]].normalized,
				navs, navNormals, neighbors, 6);
		}
		//merge Nearest Neigbors
		for (int i = 0; i < navs.Count; i++) 
		{
			for (int j = 0; j < navs.Count; j++) 
			{
				if (i != j)
				{
					if (neighbors[i].Count > 0 && (navs[i] - navs[j]).sqrMagnitude < 0.01f)
					{
						ReplaceAllIndexes(neighbors, j, i);
						navNormals[j] = Vector3.zero;
						navs[j] = Vector3.zero;
					}
				}
			}
//			navNormals.Add(navs[i].normalized);
		}
		navMeshPoints = navs.ToArray ();
		navMeshNormals = navNormals.ToArray ();	

		navMeshLinks = new int[neighbors.Count][];
		for (int i = 0; i < neighbors.Count; i++) 
		{
			navMeshLinks[i] = neighbors[i].ToArray();
		}
		meshCollider.sharedMesh = CreateColliderMesh (12);
	}

	static void ReplaceAllIndexes(List<List<int>> neighbors, int from, int to)
	{
		neighbors[to].AddRange(neighbors[from]);
		neighbors [from] = new List<int> ();
		for (int i = 0; i < neighbors.Count; i++) 
		{
			for(int j = 0; j < neighbors[i].Count; j++)
			{
				if (neighbors[i][j] == from)
				{
					neighbors[i][j] = to;
				}
			}
		}
	}

	static float NoiseFunction(Vector3 pos)
	{
		//return 0;
		pos += new Vector3 (15.2f, 122.33f, 23.12f);
//		return
//			(
//				Noise.Perlin (pos, 1.2f) * 0.3f + 
//				Noise.Perlin (pos, 2.0f) * 0.17f + 
//				Noise.Perlin (pos, 5.0f) * 0.08f + 
//				Noise.Perlin (pos, 11.0f) * 0.04f + 
//				Noise.Perlin (pos, 23.1f) * 0.021f);
		return
			(
				Noise.Perlin (pos, 1.2f) * 0.17f + 
				Noise.Perlin (pos, 2.0f) * 0.09f + 
				Noise.Perlin (pos, 5.0f) * 0.05f + 
				Noise.Perlin (pos, 11.0f) * 0.02f + 
				Noise.Perlin (pos, 23.1f) * 0.011f);
	}

	Mesh CreateColliderMesh(int details)
	{
		List<Vector3> verts = new List<Vector3>();
		List<int> faces = new List<int>();
		
		for (int i = 0; i < triangles.Length / 3; i++)
		{
			subdivideCollider(
				baseVertices[triangles[i * 3 + 0]].normalized,
				baseVertices[triangles[i * 3 + 1]].normalized,
				baseVertices[triangles[i * 3 + 2]].normalized,
				verts,
				faces,
				details);
		}
		return new Mesh { vertices = verts.ToArray(), triangles = faces.ToArray()};
	}

	Mesh CreateMesh(int details = 52)
	{
		List<Vector3> verts = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<Vector3> normals = new List<Vector3>();
		List<int> faces = new List<int>();
		List<int> normalOptimizer = new List<int> ();

		for (int i = 0; i < triangles.Length / 3; i++)
		{
			subdivide(
				baseVertices[triangles[i * 3 + 0]].normalized,
				baseVertices[triangles[i * 3 + 1]].normalized,
				baseVertices[triangles[i * 3 + 2]].normalized,
				baseUV[UVTriangles[i * 3 + 0]].normalized,
				baseUV[UVTriangles[i * 3 + 1]].normalized,
				baseUV[UVTriangles[i * 3 + 2]].normalized,
				verts,
				uv,
				normals,
				normalOptimizer,
				faces,
				details);
		}
		
		//merge Nearest Neigbors normals
		for (int i = 0; i < normalOptimizer.Count; i++) 
		{
			for (int j = 0; j < normalOptimizer.Count; j++) 
			{
				if (i != j)
				{
					if (verts[normalOptimizer[i]] == verts[normalOptimizer[j]])
					{
						Vector3 merged = ((normals[normalOptimizer[i]] + normals[normalOptimizer[j]]) * 0.5f).normalized;
						normals[normalOptimizer[i]] = merged;
						normals[normalOptimizer[j]] = merged;
						break;
					}
				}
			}
		}
		return new Mesh { vertices = verts.ToArray(), triangles = faces.ToArray(), uv = uv.ToArray(), normals = normals.ToArray() };
	}

	void subdivide(Vector3 a, Vector3 b, Vector3 c,
	                      Vector2 ta, Vector2 tb, Vector2 tc,
	                      List<Vector3> verts, List<Vector2> uv, List<Vector3> normals, List<int> normalOptimizer, List<int> faces, int level = 16)
	{
		Vector3 deltaAB = (b - a) / level;
		Vector3 deltaAC = (c - a) / level;
		Vector3 texAB = (tb - ta) / level;
		Vector3 texAC = (tc - ta) / level;

		int vertexCount = verts.Count;

		int faceCounter = verts.Count / 3;

		int[,] trianglePoints = new int[level + 1, level + 1];
		for (int n = 0; n <= level; n++) 
		{
			for (int m = 0; m <= level; m++) 
			{
				if (m + n <= level)
				{
					Vector3 point = (a + deltaAB * m + deltaAC * n).normalized;
					point = point * (size + NoiseFunction(point) * size);
					verts.Add(point);
					uv.Add(a + texAB * m + texAC * n);
					float len = (deltaAB * (0.5f)).magnitude * size;
					Vector3[] nm = new Vector3[]
					{
						(point + (deltaAB * (0.5f) + deltaAC * (-0.5f)).normalized * len).normalized,
						(point + (deltaAB * (0.5f)).normalized * len).normalized,
						(point + (deltaAC * (-0.5f)).normalized * len).normalized
					};
					Vector3 normal = GetNormal(
						nm[0] * (1f + NoiseFunction(nm[0])),
						nm[1] * (1f + NoiseFunction(nm[1])),
						nm[2] * (1f + NoiseFunction(nm[2]))
						);
					normals.Add(normal);
					trianglePoints[n, m] = vertexCount;
					if (n == 0 || m == 0 || m + n == level)
					{
						normalOptimizer.Add(vertexCount);
					}
					vertexCount ++;
				}
			}
		}
		for (int n = 0; n < level; n++) 
		{
			for (int m = 0; m < level; m++) 
			{
				if (m + n < level)
				{
					faces.Add(trianglePoints[m, n]);
					faces.Add(trianglePoints[m, n + 1]);
					faces.Add(trianglePoints[m + 1, n]);
				}
				if (m + n < level - 1)
				{
					faces.Add(trianglePoints[m + 1, n + 1]);
					faces.Add(trianglePoints[m + 1, n]);
					faces.Add(trianglePoints[m, n + 1]);
				}
			}
		}
	}

	void subdivideCollider(Vector3 a, Vector3 b, Vector3 c,
	                               List<Vector3> verts, List<int> faces, int level)
	{
		Vector3 deltaAB = (b - a) / level;
		Vector3 deltaAC = (c - a) / level;
		
		int vertexCount = verts.Count;
		
		int faceCounter = verts.Count / 3;
		
		int[,] trianglePoints = new int[level + 1, level + 1];
		for (int n = 0; n <= level; n++) 
		{
			for (int m = 0; m <= level; m++) 
			{
				if (m + n <= level)
				{
					Vector3 point = (a + deltaAB * m + deltaAC * n).normalized;
					point = point * (size + NoiseFunction(point) * size);
					verts.Add(point);
					trianglePoints[n, m] = vertexCount;
					vertexCount ++;
				}
			}
		}
		for (int n = 0; n < level; n++) 
		{
			for (int m = 0; m < level; m++) 
			{
				if (m + n < level)
				{
					faces.Add(trianglePoints[m, n]);
					faces.Add(trianglePoints[m, n + 1]);
					faces.Add(trianglePoints[m + 1, n]);
				}
				if (m + n < level - 1)
				{
					faces.Add(trianglePoints[m + 1, n + 1]);
					faces.Add(trianglePoints[m + 1, n]);
					faces.Add(trianglePoints[m, n + 1]);
				}
			}
		}
	}

	bool NodeExist(int m, int n, int maxNode)
	{
		return (m + n <= maxNode && m >= 0 && n >= 0);
	}

	void subdivideNavPoints(Vector3 a, Vector3 b, Vector3 c, List<Vector3> points, List<Vector3> normals, List<List<int>> neighbors, int level)
	{
		Vector3 deltaAB = (b - a) / level;
		Vector3 deltaAC = (c - a) / level;
		
		int pointCount = points.Count;
		int nodesCount = points.Count;
		int[,] trianglePoints = new int[level + 1, level + 1];

		for (int n = 0; n <= level; n++) 
		{
			for (int m = 0; m <= level; m++) 
			{
				if (m + n <= level)
				{
					Vector3 point = (a + deltaAB * m + deltaAC * n).normalized;
					point = point * (size + NoiseFunction(point) * size);
					points.Add(point);
					float len = (deltaAB * (0.5f)).magnitude;
					Vector3[] nm = new Vector3[]
					{
						(point + (deltaAB * (0.5f) + deltaAC * (-0.5f)).normalized * len).normalized,
						(point + (deltaAB * (0.5f)).normalized * len).normalized,
						(point + (deltaAC * (-0.5f)).normalized * len).normalized
					};
					Vector3 normal = GetNormal(
						nm[0] * (1f + NoiseFunction(nm[0])),
						nm[1] * (1f + NoiseFunction(nm[1])),
						nm[2] * (1f + NoiseFunction(nm[2]))
						);
					normals.Add((normal + point.normalized).normalized);
					neighbors.Add(new List<int>());
					trianglePoints[m, n] = pointCount;
					pointCount ++;
				}
			}
		}
		// find neighbors
		for (int n = 0; n <= level; n++) 
		{
			for (int m = 0; m <= level; m++) 
			{
				if (m + n <= level)
				{
					if (NodeExist(m + 1, n, level)) {neighbors[nodesCount].Add(trianglePoints[m + 1, n]);}
					if (NodeExist(m, n + 1, level)) {neighbors[nodesCount].Add(trianglePoints[m, n + 1]);}
					if (NodeExist(m + 1, n - 1, level)) {neighbors[nodesCount].Add(trianglePoints[m + 1, n - 1]);}
					if (NodeExist(m - 1, n + 1, level)) {neighbors[nodesCount].Add(trianglePoints[m - 1, n + 1]);}
					if (NodeExist(m - 1, n, level)) {neighbors[nodesCount].Add(trianglePoints[m - 1, n]);}
					if (NodeExist(m, n - 1, level)) {neighbors[nodesCount].Add(trianglePoints[m, n - 1]);}
					nodesCount ++;
				}
			}
		}
	}

	static Vector3 GetNormal(Vector3 a, Vector3 b, Vector3 c) {
		Vector3 d = a - b;
		Vector3 e = b - c;
		Vector3 temp = new Vector3 (
			d.y * e.z - d.z * e.y,
			d.z * e.x - d.x * e.z,
			d.x * e.y - d.y * e.x);
		return temp.normalized;
	}

	#region base icosaedr points
	const float magic = 0.5f - (3f - 2.236068f) / 4f;
	const float baseSize = 0.5f;
	static readonly int[] triangles = new[] {
		0, 1, 4,
		0, 7, 1,
		3, 2, 6,
		3, 5, 2,
		4, 5, 8,
		4, 11, 5,
		6, 7, 9,
		6, 10, 7,
		8, 9, 0,
		8, 3, 9,
		10, 11, 1,
		10, 2, 11,
		0, 4, 8,
		0, 9, 7,
		1, 11, 4,
		1, 7, 10,
		3, 8, 5,
		3, 6, 9,
		2, 5, 11,
		2, 10, 6
	};
	static readonly Vector3[] baseVertices = new Vector3[]
	{
		new Vector3 ( magic,  baseSize, 0),
		new Vector3 (-magic,  baseSize, 0),
		new Vector3 (-magic, -baseSize, 0),
		new Vector3 ( magic, -baseSize, 0),
		new Vector3 (0,  magic,  baseSize),
		new Vector3 (0, -magic,  baseSize),
		new Vector3 (0, -magic, -baseSize),
		new Vector3 (0,  magic, -baseSize),
		new Vector3 ( baseSize, 0,  magic),
		new Vector3 ( baseSize, 0, -magic),
		new Vector3 (-baseSize, 0, -magic),
		new Vector3 (-baseSize, 0,  magic)
	};
	
	static readonly int[] UVTriangles = new[] {
		0, 1, 2,
		1, 3, 2,
		2, 3, 4,
		3, 5, 4,

		0 + 6, 1 + 6, 2 + 6,
		1 + 6, 3 + 6, 2 + 6,
		2 + 6, 3 + 6, 4 + 6,
		3 + 6, 5 + 6, 4 + 6,
		
		0 + 12, 1 + 12, 2 + 12,
		1 + 12, 3 + 12, 2 + 12,
		2 + 12, 3 + 12, 4 + 12,
		3 + 12, 5 + 12, 4 + 12,

		0 + 18, 1 + 18, 2 + 18,
		1 + 18, 3 + 18, 2 + 18,
		2 + 18, 3 + 18, 4 + 18,
		3 + 18, 5 + 18, 4 + 18,
		
		0 + 24, 1 + 24, 2 + 24,
		1 + 24, 3 + 24, 2 + 24,
		2 + 24, 3 + 24, 4 + 24,
		3 + 24, 5 + 24, 4 + 24,
	};

	const float tr = 0.866f;
	static readonly Vector2[] baseUV = new Vector2[]
	{
		// 1-st
		new Vector2 (0.0f, 0f),
		new Vector2 (0.5f, tr),
		new Vector2 (1.0f, 0f),
		new Vector2 (1.5f, tr),
		new Vector2 (2.0f, 0f),
		new Vector2 (2.5f, tr),
		// 2-nd
		new Vector2 (0.0f, 1f + 0f),
		new Vector2 (0.5f, 1f + tr),
		new Vector2 (1.0f, 1f + 0f),
		new Vector2 (1.5f, 1f + tr),
		new Vector2 (2.0f, 1f + 0f),
		new Vector2 (2.5f, 1f + tr),
		// 3-rd
		new Vector2 (0.0f, 2f + 0f),
		new Vector2 (0.5f, 2f + tr),
		new Vector2 (1.0f, 2f + 0f),
		new Vector2 (1.5f, 2f + tr),
		new Vector2 (2.0f, 2f + 0f),
		new Vector2 (2.5f, 2f + tr),
		// 4-rth
		new Vector2 (0.0f, 3f + 0f),
		new Vector2 (0.5f, 3f + tr),
		new Vector2 (1.0f, 3f + 0f),
		new Vector2 (1.5f, 3f + tr),
		new Vector2 (2.0f, 3f + 0f),
		new Vector2 (2.5f, 3f + tr),
		// 5-rth
		new Vector2 (0.0f, 4f + 0f),
		new Vector2 (0.5f, 4f + tr),
		new Vector2 (1.0f, 4f + 0f),
		new Vector2 (1.5f, 4f + tr),
		new Vector2 (2.0f, 4f + 0f),
		new Vector2 (2.5f, 4f + tr),
	};
	#endregion
}
