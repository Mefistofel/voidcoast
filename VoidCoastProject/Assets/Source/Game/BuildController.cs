﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

/// <summary>
/// Build controller. Need to rotate asteroid and catch on terrain clicks
/// </summary>


public class BuildController : MonoBehaviour
{
	public AsteroidMesh asteroid = null; // set from editor 
	public GameObject lastClickedDummy = null;
	static Dictionary<int, Building> buildings = new Dictionary<int, Building> ();

	bool buildMode = false;
	Building buildPrefab = null;
	Vector3 needBuildPosition = Vector3.zero;
	Quaternion needBuildRotation = Quaternion.identity;

	void Start ()
	{
	}
		
	public void ChooseBuildingPlace(Building building)
	{
		if (buildPrefab != null)
		{
			Destroy(buildPrefab.gameObject);
		}
		buildMode = true;
		buildPrefab = building;
		needBuildPosition = Vector3.zero;
		needBuildRotation = Quaternion.identity;
		int selected = TryToSelectNavPoint (Input.mousePosition);
		if (selected >= 0)
		{
			needBuildPosition = GetNavPosition (selected);
			needBuildRotation = GetNavRotation (selected);
		}
	}

	Vector3 GetNavPosition(int selectedPoint)
	{
		return asteroid.navMeshPoints [selectedPoint];
	}
	
	Quaternion GetNavRotation(int selected)
	{
		float y = Utils.GetAngle(new Vector2(asteroid.navMeshNormals[selected].x, asteroid.navMeshNormals[selected].z));
		float x =  Utils.GetAngle(new Vector2(-asteroid.navMeshNormals[selected].y, new Vector2(asteroid.navMeshNormals[selected].x, asteroid.navMeshNormals[selected].z).magnitude));
		
		return Quaternion.Euler(0, y, 0) * Quaternion.Euler(90 + x, 0, 0);
	}

	void Build(int selected)
	{
		// pay for building
		Vector3 currentPosition = buildPrefab.transform.position;
		Vector3 needPosition = GetNavPosition (selected);
		Timer.Add (0.2f,
		(anim) =>
		{
			buildPrefab.transform.position = Vector3.Lerp(currentPosition, needPosition, anim * anim);
		},
		() => 
		{
			buildPrefab.transform.position = needPosition;
			buildPrefab = null;
		});
		buildPrefab.transform.rotation = GetNavRotation(selected);
		buildings.Add(selected, buildPrefab);
		buildPrefab.Build(asteroid.navMeshNormals[selected]);
		buildMode = false;
	}

	void Update ()
	{
		if (buildMode)
		{
			if (EventSystemManager.currentSystem.IsPointerOverGameObject())//.IsPointerOverEventSystemObject())
			{
				//over a GUI element
			}
			else 
			{
				int selected = TryToSelectNavPoint (Input.mousePosition);
				if (selected >= 0)
				{
					needBuildPosition = GetNavPosition (selected) * 1.03f;
					needBuildRotation = GetNavRotation (selected);
					if (buildPrefab != null)
					{
						buildPrefab.transform.position = buildPrefab.transform.position * 0.9f + needBuildPosition * 0.1f;
						buildPrefab.transform.rotation = Quaternion.Lerp(buildPrefab.transform.rotation, needBuildRotation, 0.1f);
						if (Input.GetMouseButtonUp(0))
						{ // Build structure if place is free
							if (!buildings.ContainsKey(selected))
							{
								Build(selected);
							}
						}
						if (Input.GetMouseButtonUp(1))
						{ // exit build mode
							buildMode = false;
							Destroy(buildPrefab.gameObject);
						}
					}
				}
			}
		}
		else
		{ // left click - select building
			if (Input.GetMouseButtonUp (0)) {
				if (EventSystemManager.currentSystem.IsPointerOverGameObject())//.IsPointerOverEventSystemObject())
				{
					//left-click over a GUI element
				}
				else 
				{
					int selected = TryToSelectNavPoint (Input.mousePosition);
					if (selected >= 0)
					{
						if (buildings.ContainsKey(selected))
						{
							GameUI.ShowBuildingInfo(buildings[selected]);
							// show build props
						}
					}
				}
			}
		}
	}

	void OnClick (Vector3 point)
	{
		if (lastClickedDummy != null) {
			lastClickedDummy.transform.position = point;
		}
		// find nearest nav point
	}

	int TryToSelectNavPoint (Vector2 screenPoint)
	{
		var ray = Camera.main.ScreenPointToRay (screenPoint);
		RaycastHit hit;
		//int mask = 1 << LayerMask.NameToLayer ("MapSelector");
		if (Physics.Raycast (ray, out hit, 40f)) {
			BuildController collider = hit.collider.gameObject.GetComponent<BuildController> ();
			if (collider != null && asteroid != null)
			{
				int nearest = 0;
				for (int i = 1; i < asteroid.navMeshPoints.Length; i++)
				{
					if ((asteroid.navMeshPoints [i] - hit.point).sqrMagnitude < (asteroid.navMeshPoints [nearest] - hit.point).sqrMagnitude) {
						nearest = i;
					}
				}
				return nearest;
			}
		}
		return -1;
	}
}
