﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// control build button on game UI build interface. - lock button if not enougth money or dont have tech level etc
/// </summary>
public class BuildButtonController : MonoBehaviour
{
	public Building controlBuilding = null;
	Button controlledButton = null;

	void SetState()
	{
		if (controlledButton != null) {
			if (controlBuilding != null) {
				bool haveEnoughtResources = true;
				foreach (var costResources in controlBuilding.cost) {
					if (Stock.resources [costResources.name] < costResources.value) {
						haveEnoughtResources = false;
					}
				}
				controlledButton.interactable = haveEnoughtResources;
			} else {
				controlledButton.interactable = false;
			}
		}
	}

	void Start ()
	{
		controlledButton = GetComponent<Button> ();
		SetState();
		Stock.Subscribe ("tech_level", (techLevel) => {
			CheckTechLevel();
		});
		Stock.Subscribe (SetState);
		CheckTechLevel ();
	}
	
	void CheckTechLevel ()
	{
		if (controlledButton != null && controlBuilding != null) {
			// have tech level
			if (Stock.resources ["tech_level"] < controlBuilding.needTechLevel) {
				// not enougth tech level
				controlledButton.gameObject.SetActive (false);
			} else {
			controlledButton.gameObject.SetActive (true);
			}
		}
	}
}
