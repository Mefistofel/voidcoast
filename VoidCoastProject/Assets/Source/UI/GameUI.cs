﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
	static GameUI instance = null;
	[SerializeField]BuildController buildController = null; // Set from editor 
	[SerializeField]Text moneyCount = null; // Set from editor
	[SerializeField]Text incomeCount = null; // Set from editor
	[SerializeField]Text energyCount = null; // Set from editor
	[SerializeField]Text peopleCount = null; // Set from editor
	[SerializeField]Text dummy = null; // Set from editor 

	[SerializeField]GameObject buildPanel = null; // Set from editor
	[SerializeField]GameObject buildButton = null; // Set from editor
	[SerializeField]GameObject backBuildButton = null; // Set from editor

	[SerializeField]GameObject buildingInfoPanel = null; // Set from editor
	[SerializeField]Text buildingName = null; // Set from editor
	[SerializeField]Text buildingDescription = null; // Set from editor
	Building currentBuilding = null;

	public void CreateBuilding(int buildingId)
	{
		BuildingType newBuilding = (BuildingType)buildingId;
		Debug.Log (" Start building " + newBuilding.ToString() );
		if (buildController != null)
		{
			buildController.ChooseBuildingPlace(Building.CreateTemplate(newBuilding));
		}
		HideBuildPanel ();
	}

	public static void ShowBuildingInfo(Building building = null)
	{
		if (instance != null && instance.buildingInfoPanel != null && instance.buildingName != null && instance.buildingDescription != null)
		{
			if (building != null)
			{
				instance.buildingInfoPanel.SetActive(true);
				instance.buildingName.text = building.name.ToLower();
				instance.buildingDescription.text = "";
			}
			else
			{
				instance.buildingInfoPanel.SetActive(false);
			}
			instance.currentBuilding = building;
		}
	}

	void Start ()
	{
		instance = this;
		UpdateMainPanel ();
		ShowBuildPanel (false);
		ShowBuildingInfo ();
//		Stock.Subscribe("money", (count) => {
//			UpdateMainPanel();
//		});
//		Stock.Subscribe("income", (count) => {
//			UpdateMainPanel();
//		});
//		Stock.Subscribe("power", (count) => {
//			UpdateMainPanel();
//		});
//		Stock.Subscribe("people", (count) => {
//			UpdateMainPanel();
//		});
//		Stock.Subscribe("house", (count) => {
//			UpdateMainPanel();
//		});
	}

	public void ShowBuildPanel (bool show = true) // Button action
	{
		if (buildPanel != null)
		{
			buildPanel.SetActive(show);
		}
		if (backBuildButton != null)
		{
			backBuildButton.SetActive(show);
		}
		if (buildButton != null)
		{
			buildButton.SetActive(!show);
		}
	}
	
	public void HideBuildPanel() // Button action
	{
		ShowBuildPanel(false);
	}


	public static void OnStateChanged()
	{
		if (instance != null && instance.dummy != null)
		{
			string resourceValues = "";
			foreach (var name in Stock.resources.resourceList.Keys)
			{
				resourceValues += name + ": " + Stock.resources[name].ToString() + "\n";
			}
			instance.dummy.text = resourceValues;
			instance.UpdateMainPanel();
		}
	}

	void Update ()
	{
	
	}

	void UpdateMainPanel ()
	{
		if (moneyCount != null)
		{
			moneyCount.text = "m: " + Stock.resources["money"].ToString();
		}
		if (incomeCount != null)
		{
			incomeCount.text = "i: " + Stock.resources["income"].ToString();
		}
		if (energyCount != null)
		{
			energyCount.text = "e: " + Stock.resources["power"].ToString();
		}
		if (peopleCount != null)
		{
			peopleCount.text = "p: " + Stock.resources["people"].ToString() + "/" + Stock.resources["house"].ToString();
		}
	}
}
